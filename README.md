# Paas-Word Ruby on Rails Middleware

[Paas-Word](https://www.paas-word.com) is an online authentication and user management service.
This Ruby on Rails Rack middleware library by [Paas-Word](https://www.paas-word.com) enables website owners with a Ruby backend to restrict their endpoints to authenticated users only and retrieve user data. 

## Usage

1. Create a free account at [Paas-Word](https://www.paas-word.com) website.
2. Recieve a login, sign-up, account and forgot-password pages for your website based on the user attributes you set up.
3. Set the callback pages on your website where users will be redirected after they sign-up and log in. 
4. Once a user is redirected to your website with a token, send this token to your backend in the "x-auth-token" header.

## Installation

Add this line to your application's Gemfile:

```
gem 'Paasword'
```

And then execute:

```
bundle
```

Or install it yourself as:

```
gem install Paasword
```

## Set Private Key as Environment Variable
Create an app on [Paas-Word](https://www.paas-word.com) and then set its Private Key as an environment variable. 

```
export PAASWORD_APP_PRIVATE_KEY=93f56f52-957d-4953-93a6-c5492e79778b
```

## Guard all endpoints
In config/environments/developments.rb add:

```ruby
config.middleware.use Paasword
```

## Guard specific routes against unauthenticated users

Divide your routes into different modules and add "middleware.use Paasword" to relevant modules.
[See this answer as an example](https://stackoverflow.com/questions/33982663/build-2-middleware-stacks-in-rails-app/41515577#41515577)

## Retrieve user information

```ruby
class WelcomeController < ApplicationController
  def index
  	@user = request.env['user']
    render json: @user
  end
end
```

