require 'jwt'

class Paasword
	def initialize(app)
		@app = app
	end

	def call(env)
		begin
			@req = Rack::Request.new(env)

			token = env['HTTP_X_AUTH_TOKEN']
			appPrivateKey = ENV['PAASWORD_APP_PRIVATE_KEY']
			if !token
				error("MISSING_ELEMENT", "x-auth-token")
			elsif !appPrivateKey
				error("MISSING_ELEMENT", "PAASWORD_APP_PRIVATE_KEY")
			else 
				user = JWT.decode token, appPrivateKey, true
				user = user[0]

				if user && !user['AutoLogout']['IsEnabled']
					loginTime = user['iat'];
					hoursSinceLogin = ((Time.now.to_i - loginTime)/3600).round;
					limit = user['AutoLogout']['LogoutEveryXHours']
					if hoursSinceLogin > limit
						error("SESSION_EXPIRED", "")
					end
				end

				env['user'] = user

				status, headers, body = @app.call(env)
				[status, headers, body]
			end
		rescue Exception => ex
			error("INTERNAL_ERROR", ex.message)
		end
	end

	def error(errorType, errorMessage)
		body = { "ErrorType" => errorType, "ErrorMessage" => errorMessage }
		[401, { 'Content-Type' => 'application/json; charset=utf-8' }, [ body.to_json ]]
	end

end