Gem::Specification.new do |s|
  s.name        = 'Paasword'
  s.version     = '1.0.1'
  s.date        = '2019-02-26'
  s.summary     = "Paas-Word is an online authentication and user management service.
This Ruby on Rails Rack middleware library by Paas-Word enables website owners with a Ruby backend to restrict their endpoints to authenticated users only and retrieve user data."
  s.description = "Paas-Word is an online authentication and user management service.
This Ruby on Rails Rack middleware library by Paas-Word enables website owners with a Ruby backend to restrict their endpoints to authenticated users only and retrieve user data."
  s.authors     = ["Gilad Soffer"]
  s.email       = 'paasword.cto@gmail.com'
  s.files       = ["lib/paasword.rb"]
  s.homepage    = 'http://rubygems.org/gems/paasword'
  s.license     = 'MIT'
  s.add_dependency 'jwt'
end